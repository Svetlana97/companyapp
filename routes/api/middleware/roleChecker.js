const ROLE_PERMISSIONS = require("../../../config/constants").ROLE_PERMISSIONS;

const roleHasPermission = (role, permission) => {
  return ROLE_PERMISSIONS[role].includes(permission);
};

exports.allowOnlyRole = (allowedRole, callback) => {
  return function(req, res) {
    var allowed = false;
    if (Array.isArray(allowedRole)) {
      allowed = allowedRole.includes(req.user.role);
    } else {
      allowed = allowedRole === req.user.role;
    }
    if (allowed) {
      callback(req, res);
    } else {
      res.sendStatus(403);
      return;
    }
  };
};

exports.allowOnlyPermission = (permission, callback) => {
  return function(req, res) {
    if (roleHasPermission(req.user.role, permission)) {
      callback(req, res);
    } else {
      res.sendStatus(403);
      return;
    }
  };
};