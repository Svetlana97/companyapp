const express = require('express');
const router = express.Router();
const passport = require("passport");
const allowOnlyPermission = require('./middleware/roleChecker').allowOnlyPermission;
const constants = require('../../config/constants');
const ObjectId = require("mongoose").Types.ObjectId;

//Loading validation
const validateCompanyInput = require("../../validation/companies");

//Loading models
const Company = require("../../models/Company");

// route - GET api/companies/test
// desc  - Tests post route
// access - Public
router.get("/test", (req, res) => {
  console.log(constants.PERMISSIONS.PERMISSION_VIEW_COMPANY);
  res.json({ msg: "Companies works!" });
});

// route - GET api/companies/
// desc - Find All Companies
// access - Private (User, Admin)

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_VIEW_COMPANY, (req, res) => {
    let errors = {};
    Company.find()
      .populate('City', ['name'])
      .then(companies => {
        if (companies.length === 0) {
          errors.no_companies = "There are no companies";
          return res.status(400).json(errors);
        }
        return res.status(200).json(companies);
      })
      .catch(err => {
        return res.status(400).json({
          msg: "There was an error fetching companies from database",
          err: err
        });
      });

  })
);

// route - GET api/companies/cities/:city_id
// desc - Find Companies By City Id
// access - Private (User, Admin)

router.get(
  "/cities/:city_id",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_VIEW_COMPANY, (req, res) => {
    let errors = {};
    Company
      .find({ city_id: ObjectId(req.params.city_id) })
      .populate('City', ['name'])
      .then(companies => {
        if (companies.length === 0) {
          errors.no_companies = "There are no companies";
          return res.status(400).json(errors);
        }

        return res.status(200).json(companies);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching companies from database",
          err: err
        });
      });
  })
);

// route   GET api/companies/:_id
// desc    Find Company By Id
// access  Private (User, Admin)
router.get(
  "/:_id",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_VIEW_COMPANY, (req, res) => {
    let errors = {};
    Company
      .findById(req.params._id)
      .populate('City', ['name'])
      .then(company => {
        if (!company) {
          errors.no_company = "Requested company does not exists";
          return res.status(400).json(errors);
        }

        return res.status(200).json(company);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching company from database",
          err: err
        });
      });
  })
);

// route - POST api/companies/
// desc - Create Or Update A Company
// access - Private (User, Admin)
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_MANAGE_COMPANY, (req, res) => {
    const { errors, isValid } = validateCompanyInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    let companyFields = {};
    companyFields.name = req.body.name;
    companyFields.address = req.body.address;
    companyFields.email = req.body.email;
    companyFields.website = req.body.website;
    companyFields.latitude = req.body.latitude;
    companyFields.longitude = req.body.longitude;
    companyFields.city_id = req.body.city_id;

    Company
      .findById(req.params._id)
      .then(company => {
        if (company) {
          Company
            .findByIdAndUpdate(req.params._id, { $set: company }, { new: true })
            .then(company => {
              return res.status(200).json(company);
            })
            .catch(err => {
              return res.status(404).json({
                msg: "There was an error updating company",
                err: err
              });
            });
        } else {
          new Company(companyFields)
            .save()
            .then(company => {
              return res.status(200).json(company);
            })
            .catch(err => {
              return res.status(404).json({
                msg: "There was an error saving new Company",
                err: err
              });
            });
        }
      });
  })
);

// route   DELETE api/companies/:_id
// desc    Delete Company By Id
// access  Private (User, Admin)
router.delete(
  "/:_id",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_DELETE_COMPANY, (req, res) => {
    Company.findByIdAndDelete(req.params._id)
      .then((result) => {
        if (result) {
          return res.status(200).json({ msg: "Company removed" });
        }
        return res.status(400).json({ msg: "Company does not exists" });
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error removing company",
          err: err
        });
      });
  })
);

module.exports = router;