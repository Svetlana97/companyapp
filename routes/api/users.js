const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");
const allowOnlyRole = require('./middleware/roleChecker').allowOnlyRole;
const constants = require('../../config/constants');

// Load User model
const User = require("../../models/User");

// Load Input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// route - GET api/users/test
// desc - Tests post route
// access - Public
router.get("/test", (req, res) => {
  res.json({ msg: "Users works" });
});

// route -  POST api/users/register
// desc -  Register an User
// access - Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email })
    .then(user => {
      if (user) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) {
              throw err;
            }

            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                return res.status(200).json(user);
              })
              .catch(err => {
                return res.status(404).json({ error: err });
              });
          });
        });
      }
    })
    .catch(err => {
      return res.status(404).json({ error: err });
    });
});

// route - POST api/users/login
// desc -  Login an User/Returning JWT Token
// access -  Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find an user by email
  User.findOne({ email: email })
    .then(user => {
      // Check for user
      if (!user) {
        errors.msg = "User not found";
        return res.status(404).json(errors);
      }

      // Check Password
      bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          // User Matched

          // Create JWT Payload
          const payload = {
            id: user.id,
            name: user.name
          };

          // Sign Token
          jwt.sign(
            payload,
            keys.jwtSecret, { expiresIn: 86400 },
            (err, token) => {
              res.json({
                success: true,
                token: 'Bearer ' + token
              });
            }
          );
        } else {
          errors.password = "Password incorrect";
          return res.status(400).json(errors);
        }
      });
    });
});

// route - GET api/users/current
// desc  - Return current user
// access - Private (Users and Admins)
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  allowOnlyRole([constants.ROLE_USER, constants.ROLE_ADMIN], (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      role: req.user.role
    });
  })
);

module.exports = router;