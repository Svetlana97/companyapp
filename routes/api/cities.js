const express = require('express');
const router = express.Router();
const passport = require("passport");
const allowOnlyPermission = require('./middleware/roleChecker').allowOnlyPermission;
const constants = require('../../config/constants');
const ObjectId = require("mongoose").Types.ObjectId;

//Load validation
const validateCityInput = require('../../validation/cities');

//Load Models
const City = require('../../models/City');
const Company = require('../../models/Company');

// route - Get api/cities/test
// Access public
router.get("/test", (req, res) => {
  res.json({
    msg: "Cities works!"
  });
});

// route -  GET api/cities/
// desc  -  Find All cities
// access - Private (User, Admin)

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_VIEW_CITY, (req, res) => {
    let errors = {};
    City.find()
      .then(cities => {
        if (cities.length === 0) {
          errors.no_cities = "There is no cities";
          return res.status(400).json(errors);
        }
        return res.status(200).json(cities);
      })
      .catch(err => {
        return res.status(400).json({
          msg: "There was an error fetching categories from database",
          err: err
        });
      });
  })
);

// route -  GET api/cities/:city_id
// desc  -  Find City By Id
// access  - Private (User, Admin)

router.get(
  "/:city_id",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_VIEW_CITY, (req, res) => {
    let errors = {};
    City
      .findById(req.params.city_id)
      .then(city => {
        if (!city) {
          errors.no_city = "Requested city does not exists";
          return res.status(400).json(errors);
        }
        return res.status(200).json(city);
      })
      .catch(err => {
        return res.status(400).json({
          msg: "There was an error fetching categories from database",
          err: err
        });
      });
  })
);

// route - POST api/cities/
// desc  -  Create Or Update City
// access -  Private (Admin)

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_MANAGE_CITY, (req, res) => {
    const { errors, isValid } = validateCityInput(req.body);

    //Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    let cityFields = {};
    cityFields.name = req.body.name;

    City.findById(req.params.city_id)
      .then(city => {
        if (city) {
          City
            .findByIdAndUpdate(req.params._id, { $set: cityFields }, { new: true })
            .then(city => {
              return res.status(200).json(city);
            })
            .catch(err => {
              return res.status(400).json({
                msg: "There was an error updating city",
                err: err
              });
            });
        } else {
          new City(cityFields)
            .save()
            .then(city => {
              return res.status(200).json(city);
            })
            .catch(err => {
              return res.status(400).json({
                msg: "There was an error saving new City",
                err: err
              });
            });
        }
      });
  })
);

// route - DELETE api/city/:city_id
// desc -   Delete City By Id
// access - Private (Admin)

router.delete(
  "/:city_id",
  passport.authenticate("jwt", { session: false }),
  allowOnlyPermission(constants.PERMISSIONS.PERMISSION_DELETE_CITY, (req, res) => {
    //Remove all companies which are related to that city
    Company
      .deleteMany({ 'city_id': ObjectId(req.param.city_id) })
      .then(() => {
        City
          .findByIdAndDelete(req.params.city_id)
          .then((result) => {
            if (result) {
              return res.status(200).json({ msg: "City removed" });
            }
            return res.status(400).json({ msg: "City does not exists" });
          })
          .catch(err => {
            return res.status(400).json({
              msg: "There was an error removing city",
              err: err
            });
          });
      })
      .catch(err => {
        return res.status(400).json({
          msg: "There was an error removing companies",
          err: err
        });
      });
  })
);

module.exports = router;