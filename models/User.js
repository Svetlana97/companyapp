const mongoose = require("mongoose");
const constants = require('../config/constants');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    allowNull: false
  },
  email: {
    type: String,
    unique: true,
    required: true,
    allowNull: false
  },
  password: {
    type: String,
    required: true,
    allowNull: false
  },
  role: {
    type: String,
    default: constants.ROLE_USER,
    required: true,
    allowNull: false
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }
});

module.exports = mongoose.model("User", UserSchema);