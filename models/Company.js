const mongoose = require("mongoose");
const Schema = mongoose.Schema;


//Create Schema
const CompanySchema = new Schema({
  name: {
    type: String,
    required: true,
    allowNull: false
  },
  city_id: {
    type: Schema.Types.ObjectId,
    required: true,
    allowNull: false,
    ref: 'City'
  },
  address: {
    type: String,
    required: true,
    allowNull: false
  },
  email: {
    type: String,
    required: false,
    default: null
  },
  website: {
    type: String,
    required: false,
    default: null
  },
  latitude: {
    type: Number,
    required: true,
    allowNull: false
  },
  longitude: {
    type: Number,
    required: true,
    allowNull: false
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }

});

module.exports = mongoose.model("Company", CompanySchema);