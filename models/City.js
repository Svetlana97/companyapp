const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CitySchema = new Schema({
  name: {
    type: String
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }
})

module.exports = mongoose.model("City", CitySchema, 'cities');