const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateCompanyInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.address = !isEmpty(data.address) ? data.address : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required";
  }

  if (Validator.isEmpty(data.address)) {
    errors.address = "Address field is required";
  }

  if (data.email !== null && !(typeof data.email === 'string' && Validator.isEmail(data.email))) {
    errors.email = 'Email field is invalid';
  }

  if (data.website !== null && !(typeof data.website === 'string' && Validator.isURL(data.website))) {
    errors.website = 'Website field is invalid';
  }

  if (data.latitude < -90 || data.latitude > 90) {
    errors.latitude = 'The latitude must be a number between -90 and 90';
  }

  if (data.longitude < -180 || data.longitude > 180) {
    errors.longitude = 'The longitude must be a number between -180 and 180';
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};